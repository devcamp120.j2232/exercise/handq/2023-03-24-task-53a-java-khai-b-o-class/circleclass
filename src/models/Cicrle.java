package models;

public class Cicrle {
    private double radius = 1.0;

    public Cicrle() {
    }

    public Cicrle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    // diện tích hình tròn
    public double getArea() {
        return Math.pow(this.radius, 2) * Math.PI;
    }

    // tính chu vi hình tròn
    public double getCircumference() {
        return 2 * Math.PI * this.radius;
    }

    // trả lại string
    public String toString() {
        return "Circle[radius=" + this.radius + "]";
    }
}
