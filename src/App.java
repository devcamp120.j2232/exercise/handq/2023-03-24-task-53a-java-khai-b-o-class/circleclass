import models.Cicrle;

public class App {
    public static void main(String[] args) throws Exception {
        Cicrle cicrle1 = new Cicrle();

        Cicrle cicrle2 = new Cicrle(3.0);

        System.out.println("Cicrle 1");
        System.out.println(cicrle1.getRadius());
        System.out.println(cicrle1.getArea());
        System.out.println(cicrle1.getCircumference());
        System.out.println(cicrle1.toString());

        System.out.println("Cicrle 2");
        System.out.println(cicrle2.getRadius());
        System.out.println(cicrle2.getArea());
        System.out.println(cicrle2.getCircumference());
        System.out.println(cicrle2.toString());

    }
}
